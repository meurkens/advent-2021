use std::collections::HashMap;

fn parse(input: &str) -> HashMap<&str, Vec<&str>> {
    let mut result = HashMap::new();

    for l in input.lines() {
        let mut fields = l.split('-');
        let from = fields.next().unwrap();
        let to = fields.next().unwrap();

        let from_ = result.entry(from).or_insert_with(Vec::new);
        from_.push(to);

        let to_ = result.entry(to).or_insert_with(Vec::new);
        to_.push(from);
    }

    result
}

fn add<'a>(path: &[&'a str], new: &'a str) -> Vec<&'a str> {
    let mut result = path.to_owned();
    result.push(new);
    result
}

fn find_paths<'a>(
    map: &HashMap<&'a str, Vec<&'a str>>,
    allow_doubled: bool,
) -> Vec<Vec<&'a str>> {
    let mut queue = vec![(vec!["start"], false)];
    let mut paths = Vec::new();
    
    while !queue.is_empty() {
        let (path, doubled) = queue.pop().unwrap();
        let targets = &map[path.last().unwrap()];
        for &t in targets {
            if t == "start" {
                continue;
            }

            if t == "end" {
		paths.push(add(&path, t));
            } else if t.to_uppercase() == *t || !path.contains(&t) {
                queue.push((add(&path, t), doubled));
            } else if allow_doubled && !doubled {
                queue.push((add(&path, t), true));
            }
        }
    }
    paths
}

pub fn part_a(input: &str) -> String {
    find_paths(&parse(input), false).len().to_string()
}

pub fn part_b(input: &str) -> String {
    find_paths(&parse(input), true).len().to_string()
}

#[cfg(test)]
mod tests {
    use super::super::utils;
    use super::*;

    #[test]
    fn run() {
        println!("{}", part_a(&utils::input("day_12")));
        println!("{}", part_b(&utils::input("day_12")));
    }

    #[test]
    fn test_parse_input() {
        let input = "start-a\na-end";
        let map = parse(input);

        assert_eq!(
            map,
            HashMap::from([
                ("a", vec!["start", "end"]),
                ("start", vec!["a"]),
                ("end", vec!["a"])
            ])
        );
    }

    #[test]
    fn test_find_path() {
        let path = HashMap::from([
            ("a", vec!["start", "end"]),
            ("b", vec!["start", "end"]),
            ("start", vec!["a", "b"]),
            ("end", vec!["a", "b"]),
        ]);

        assert_eq!(
            find_paths(&path, false),
            vec![vec!["start", "b", "end"], vec!["start", "a", "end"]]
        );

        let path = HashMap::from([
            ("A", vec!["start", "end", "b"]),
            ("b", vec!["A"]),
            ("start", vec!["A"]),
            ("end", vec!["A"]),
        ]);

        assert_eq!(find_paths(&path, false).len(), 2);
    }

    #[test]
    fn test_find_path_b() {
        let path = HashMap::from([
            ("a", vec!["start", "end"]),
            ("b", vec!["start", "end"]),
            ("start", vec!["a", "b"]),
            ("end", vec!["a", "b"]),
        ]);

        assert_eq!(
            find_paths(&path, true),
            vec![vec!["start", "b", "end"], vec!["start", "a", "end"]]
        );

        let path = HashMap::from([
            ("A", vec!["start", "end", "b"]),
            ("b", vec!["A"]),
            ("start", vec!["A"]),
            ("end", vec!["A"]),
        ]);

        assert_eq!(find_paths(&path, true).len(), 3);
    }

    #[test]
    fn test_part_a() {
        let input = "dc-end
HN-start
start-kj
dc-start
dc-HN
LN-dc
HN-end
kj-sa
kj-HN
kj-dc";
        assert_eq!(part_a(input), "19");
    }

    #[test]
    fn test_part_b() {
        let input = "start-A
start-b
A-c
A-b
b-d
A-end
b-end";
        assert_eq!(part_b(input), "36");
    }
}
