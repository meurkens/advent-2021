use std::{cell::RefCell, rc::Rc, fmt};

use itertools::Itertools;
use regex::Regex;

#[derive(Debug, Clone)]
enum Node {
    Value(i32),
    Pair(Rc<RefCell<Node>>, Rc<RefCell<Node>>),
}

impl Node {
    fn parse(input: &str) -> (Self, String) {
        let re_value = Regex::new(r"^(\d+)(.*)").unwrap();
        if let Some(captured) = re_value.captures_iter(input).next() {
            return (
                Self::Value(captured[1].parse().unwrap()),
                captured[2].to_string(),
            );
        }

        let input = &input[1..]; // [
        let (left, input) = Self::parse(input);
        let input = &input[1..]; // ,
        let (right, input) = Self::parse(input);
        let rest = &input[1..]; // ]

        let node = Self::Pair(Rc::new(RefCell::new(left)), Rc::new(RefCell::new(right)));

        (node, rest.to_string())
    }

    fn add(&mut self, n: i32) {
        match self {
            Self::Value(val) => {
                *self = Self::Value(*val + n);
            }
            Self::Pair(l, _) => {
                let mut left = l.borrow_mut();
                left.add(n);
            }
        }
    }

    fn val(&self) -> i32 {
        match self {
            Self::Value(val) => *val,
            Self::Pair(l, _) => l.borrow().val(),
        }
    }

    fn split(&self) -> Node {
        match self {
            Self::Pair(_, _) => {
                panic!();
            }
            Self::Value(val) => {
                let left = val / 2;
                let right = val - left;
                Node::Pair(
                    Rc::new(RefCell::new(Node::Value(left))),
                    Rc::new(RefCell::new(Node::Value(right))),
                )
            }
        }
    }

    fn magnitude(&self) -> i32 {
        match self {
            Self::Value(val) => *val,
            Self::Pair(l, r) => 3 * l.borrow().magnitude() + 2 * r.borrow().magnitude(),
        }
    }

    fn print(&self) -> String {
        match self {
            Self::Value(val) => val.to_string(),
            Self::Pair(l, r) => format!("[{},{}]", l.borrow().print(), r.borrow().print()),
        }
    }
}

impl fmt::Display for Node {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.print()) 
    }
}


#[derive(Debug)]
struct Tree {
    root: Rc<RefCell<Node>>,
}

impl Tree {
    fn new(input: &str) -> Self {
        let (node, _) = Node::parse(input);
        let root = Rc::new(RefCell::new(node));

        Self { root }
    }

    fn to_vec(&self) -> Vec<Rc<RefCell<Node>>> {
        let mut queue = vec![(self.root.clone(), 0)];
        let mut result = vec![];

        while let Some((node, depth)) = queue.pop() {
            match &*node.borrow() {
                Node::Value(_) => result.push(node.clone()),
                Node::Pair(l, r) => {
                    if depth >= 4 {
                        result.push(node.clone())
                    } else {
                        queue.push((r.clone(), depth + 1));
                        queue.push((l.clone(), depth + 1));
                    }
                }
            };
        }

        result
    }

    fn add(self, other: Self) -> Self {
        let tree = Tree {
            root: Rc::new(RefCell::new(Node::Pair(self.root, other.root))),
        };
        tree.reduce();
        tree
    }

    fn reduce(&self) {
//        println!("{}", self.print());
        let mut last: Option<Rc<RefCell<Node>>> = None;
        let mut to_add = None;
        let mut to_split = None;

        for node in self.to_vec() {
            if let Some(v) = to_add {
                node.borrow_mut().add(v);
                self.reduce();
                return;
            }

            if let Node::Pair(l, r) = &*node.borrow() {
                if let Some(last_node) = last {
                    last_node.borrow_mut().add(l.borrow().val());
                }
                to_add = Some(r.borrow().val());
            }

            if to_split.is_none() {
                if let Node::Value(v) = &*node.borrow() {
                    if *v >= 10 {
                        to_split = Some(node.clone());
                    }
                }
            }

            if to_add.is_some() {
                node.replace(Node::Value(0));
            }

            last = Some(node.clone());
        }

	if to_add.is_some() {
	    self.reduce();
	    return;
	}

        if let Some(to_split) = to_split {
            to_split.replace_with(|n| n.split());
            self.reduce();
        }
    }
}

impl fmt::Display for Tree {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.root.borrow())
    }
}

fn combine(input: &str) -> Tree {
    input
        .lines()
        .map(Tree::new)
        .reduce(|acc, item| acc.add(item))
        .unwrap()
}

pub fn part_a(input: &str) -> String {
    let result = combine(input);

    let magnitude = result.root.borrow().magnitude();
    magnitude.to_string()
}

pub fn part_b(input: &str) -> String {
    input.lines()
        .cartesian_product(input.lines())
        .map(|(a,b)| {
	    let tree = Tree::new(a).add(Tree::new(b));
	    let result = tree.root.borrow().magnitude();
	    result
	})
        .max()
        .unwrap()
        .to_string()
}

#[cfg(test)]
mod tests {
    use super::*;
    use super::super::utils;

    #[test]
    fn run() {
	let input = &utils::input("day_18");
	println!("{}", part_a(input));
	println!("{}", part_b(input));
    }

    #[test]
    fn test_reduce() {
        let tree = Tree::new("[[[[[9,8],1],2],3],4]");
        tree.reduce();

        for node in tree.to_vec() {
            println!("{:?}", node);
        }
    }

    #[test]
    fn test_split() {
        let tree = Tree::new("11");
        tree.reduce();
        println!("{}", tree);
    }

    #[test]
    fn test_example() {
        let tree_1 = Tree::new("[[[[4,3],4],4],[7,[[8,4],9]]]");
        let tree_2 = Tree::new("[1,1]");
        let tree = tree_1.add(tree_2);
        println!("{}", tree);
    }

    #[test]
    fn test_reduce_failure() {
        let input = "[[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]],[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]]";
        let tree = Tree::new(input);
        tree.reduce();
        println!("{}", tree);
    }

    #[test]
    fn test_add_list() {
        let input = "[1,1]
[2,2]
[3,3]
[4,4]
[5,5]
[6,6]";

        println!("{}", combine(input));

        let input = "[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]
[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]
[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]
[[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]
[7,[5,[[3,8],[1,4]]]]
[[2,[2,2]],[8,[8,1]]]
[2,9]
[1,[[[9,3],9],[[9,0],[0,7]]]]
[[[5,[7,4]],7],1]
[[[[4,2],2],6],[8,7]]";

        let mut lines = input.lines();
        let mut first = Tree::new(lines.next().unwrap());
        for l in lines {
            let new = Tree::new(l);
            println!("{}\n{}\n\n", first, new);
            first = first.add(new);
        }

        println!("{}", combine(input));
    }

    #[test]
    fn test_part_a() {
        let input = "[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
[[[5,[2,8]],4],[5,[[9,9],0]]]
[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
[[[[5,4],[7,7]],8],[[8,3],8]]
[[9,3],[[9,9],[6,[4,9]]]]
[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]";
        println!("{}", combine(input));
	println!("{}", part_a(input));
	println!("{}", part_b(input));
    }

    #[test]
    fn test_part_b() {
	let input = "[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
[[[5,[2,8]],4],[5,[[9,9],0]]]
[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
[[[[5,4],[7,7]],8],[[8,3],8]]
[[9,3],[[9,9],[6,[4,9]]]]
[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]";

	println!("{}", part_b(input));
    }
}
