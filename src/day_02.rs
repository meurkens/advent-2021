enum Instruction {
    Down(i32),
    Up(i32),
    Forward(i32),
}

fn instructions(input: &str) -> Vec<Instruction> {
    input
        .lines()
        .map(|l| l.split(' ').collect::<Vec<_>>())
        .map(|l| (l[0], l[1].parse().unwrap()))
        .map(|(i, n)| match i {
            "down" => Instruction::Down(n),
            "up" => Instruction::Up(n),
            "forward" => Instruction::Forward(n),
            _ => panic!(),
        })
        .collect::<Vec<_>>()
}

pub fn part_a(input: &str) -> String {
    let (rx, ry): (i32, i32) = instructions(input)
        .iter()
        .fold((0, 0), |(x, y), i| match i {
            Instruction::Down(n) => (x, y + n),
            Instruction::Up(n) => (x, y - n),
            Instruction::Forward(n) => (x + n, y),
        });

    (rx * ry).to_string()
}

pub fn part_b(input: &str) -> String {
    let (rx, ry, _ra): (i32, i32, i32) =
        instructions(input)
            .iter()
            .fold((0, 0, 0), |(x, y, a), i| match i {
                Instruction::Down(n) => (x, y, a + n),
                Instruction::Up(n) => (x, y, a - n),
                Instruction::Forward(n) => (x + n, y + (n * a), a),
            });

    (rx * ry).to_string()
}

/*
    let input_demo = "forward 5
down 5
forward 8
up 3
down 8
forward 2
";

*/
