use std::collections::HashMap;

type Rules = HashMap<(char, char), char>;

fn parse(input: &str) -> (&str, Rules) {
    let (template, rules) = input.split_once("\n\n").unwrap();

    let rules = rules
        .lines()
        .map(|l| l.split_once(" -> ").unwrap())
        .map(|(a, b)| {
            let mut c = a.chars();
            let l = c.next().unwrap();
	    let r = c.next().unwrap();
	    
            ((l, r), b.chars().next().unwrap())
        })
        .collect();

    (template, rules)
}

type Counts = HashMap<char, i64>;

fn counts_from(rules: &Rules) -> HashMap<(char, char), Counts> {
    rules
        .keys()
        .map(|(l, r)| ((*l, *r), HashMap::from([(*l, 1)])))
        .collect()
}

fn combine(l: Counts, r: Counts) -> Counts {
    let mut result = l;
    for (k, v) in r {
        *result.entry(k).or_insert(0) += v;
    }
    result
}

fn next_counts(
    counts: &HashMap<(char, char), Counts>,
    rules: &Rules,
) -> HashMap<(char, char), Counts> {
    rules
        .iter()
        .map(|(&(l, r), &middle)| {
            let first = counts[&(l, middle)].to_owned();
            let second = counts[&(middle, r)].to_owned();
            ((l, r), combine(first, second))
        })
        .collect()
}

fn run(input: &str, n: i32) -> i64 {
    let (template, rules) = parse(input);
    let mut counts = counts_from(&rules);

    for _ in 1..=n {
        counts = next_counts(&counts, &rules);
    }

    let mut all_counts = template
        .chars()
        .zip(template.chars().skip(1))
        .map(|x| counts[&x].clone())
        .reduce(combine)
        .unwrap();

    *all_counts
        .entry(template.chars().last().unwrap())
        .or_insert(0) += 1;

    all_counts.values().max().unwrap() - all_counts.values().min().unwrap()
}

pub fn part_a(input: &str) -> String {
    run(input, 10).to_string()
}

pub fn part_b(input: &str) -> String {
    run(input, 40).to_string()
}


#[cfg(test)]
mod tests {
    use super::super::utils;
    use super::*;

    #[test]
    fn run() {
        assert_eq!(part_a(&utils::input("day_14")), "5656");
        assert_eq!(part_b(&utils::input("day_14")), "12271437788530");
    }

    const TEST_INPUT: &str = "NNCB

CH -> B
HH -> N";

    #[test]
    fn test_parse() {
        let (template, rules) = parse(TEST_INPUT);
        assert_eq!(template, "NNCB");
        assert_eq!(
            rules,
            HashMap::from([
                (('C', 'H'), 'B'),
                (('H', 'H'), 'N')
            ])
        );
    }

    const DEMO_INPUT: &str = "NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C";

    #[test]
    fn test_part_a() {
        assert_eq!(part_a(DEMO_INPUT), "1588")
    }
}
