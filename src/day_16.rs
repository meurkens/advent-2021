use itertools::Either;

#[derive(PartialEq, Debug)]
struct Packet {
    version: i32,
    type_id: i32,
    data: Either<Vec<Packet>, i64>,
}

impl Packet {
    fn version_sum(&self) -> i32 {
        let added = match &self.data {
            Either::Right(_) => 0,
            Either::Left(packets) => packets.iter().map(|p| p.version_sum()).sum(),
        };
        self.version + added
    }

    fn run(&self) -> i64 {
        if let Either::Right(v) = self.data {
            return v;
        }

        if let Either::Left(packets) = &self.data {
            let mut data = packets.iter().map(|p| p.run());

            return match self.type_id {
                0 => data.sum(),
                1 => data.product(),
                2 => data.min().unwrap(),
                3 => data.max().unwrap(),
                5 => {
                    if data.next() > data.next() {
                        1
                    } else {
                        0
                    }
                }
                6 => {
                    if data.next() < data.next() {
                        1
                    } else {
                        0
                    }
                }
                7 => {
                    if data.next() == data.next() {
                        1
                    } else {
                        0
                    }
                }

                _ => panic!("noop"),
            };
        }

        0
    }
}

fn hex_to_bin(input: &str) -> String {
    let mut result = String::new();
    for i in input.chars() {
        let int = i64::from_str_radix(&i.to_string(), 16).unwrap();
        let to_add = format!("{:04b}", int);
        result.push_str(&to_add);
    }
    result
}

fn parse_packet(input: &str) -> (Packet, &str) {
    let (version, input) = input.split_at(3);
    let (type_id, mut input) = input.split_at(3);
    let version = i32::from_str_radix(version, 2).unwrap();
    let type_id = i32::from_str_radix(type_id, 2).unwrap();

    let mut data = String::new();

    if type_id == 4 {
        let mut finished = false;

        while !finished {
            let (current, rest) = input.split_at(5);
            input = rest;

            let (head, body) = current.split_at(1);
            data.push_str(body);
            if head == "0" {
                finished = true
            };
        }

        return (
            Packet {
                version,
                type_id,
                data: Either::Right(i64::from_str_radix(&data, 2).unwrap()),
            },
            input,
        );
    }

    let mut packets = Vec::new();
    let (length_type_id, input_) = input.split_at(1);
    input = input_;
    let length;
    let starting_size = input.len();

    if length_type_id == "0" {
        let (length_, input_) = input.split_at(15);
        length = usize::from_str_radix(length_, 2).unwrap();
        input = input_;
    } else {
        let (length_, input_) = input.split_at(11);
        length = usize::from_str_radix(length_, 2).unwrap();
        input = input_;
    }

    while (length_type_id == "0" && starting_size - input.len() - 15 < length)
        || (length_type_id == "1" && packets.len() < length)
    {
        let (packet, new_input) = parse_packet(input);
        input = new_input;
        packets.push(packet);
    }

    (
        Packet {
            version,
            type_id,
            data: Either::Left(packets),
        },
        input,
    )
}

pub fn part_a(input: &str) -> String {
    let (packet, _) = parse_packet(&hex_to_bin(input));
    packet.version_sum().to_string()
}

pub fn part_b(input: &str) -> String {
    let (packet, _) = parse_packet(&hex_to_bin(input));
    packet.run().to_string()
}

#[cfg(test)]
mod tests {
    use super::super::utils;
    use super::*;


    
    #[test]
    fn run() {
	let (packet, _) = parse_packet(&hex_to_bin(&utils::input("day_16")));
	
        println!(
            "{}",
            
            packet.version_sum());

	println!(
	    "{}",
	    packet.run()
		);
    }

    #[test]
    fn test_part_b() {
	assert_eq!(
	    parse_packet(&hex_to_bin("CE00C43D881120")).0.run(),
	    9);
    }

    #[test]
    fn test_part_a() {
        assert_eq!(
            parse_packet(&hex_to_bin("C0015000016115A2E0802F182340"))
                .0
                .version_sum(),
            23
        );
    }

    #[test]
    fn test_version_sum() {
        assert_eq!(
            Packet {
                version: 3,
                type_id: 4,
                data: Either::Left(vec![Packet {
                    version: 12,
                    type_id: 2,
                    data: Either::Right(0)
                }])
            }
            .version_sum(),
            15
        );
    }

    #[test]
    fn test_parse_packet() {
        assert_eq!(
            parse_packet("110100101111111000101000"),
            (
                Packet {
                    version: 6,
                    type_id: 4,
                    data: Either::Right(2021)
                },
                "000"
            )
        );

        let (packet, _) = parse_packet("00111000000000000110111101000101001010010001001000000000");

        assert_eq!(packet.version, 1);
        assert_eq!(packet.data.unwrap_left().len(), 2);

        let (packet, _) = parse_packet("11101110000000001101010000001100100000100011000001100000");

        assert_eq!(packet.data.unwrap_left().len(), 3);
    }

    #[test]
    fn test_hex_to_bin() {
        assert_eq!(hex_to_bin("0"), "0000");
        assert_eq!(hex_to_bin("A"), "1010");
        assert_eq!(hex_to_bin("F"), "1111");
        assert_eq!(hex_to_bin("A4"), "10100100");
    }
}
