use std::collections::HashSet;

use itertools::Itertools;

type State = [i64; 4];

fn index_for(attr: &str) -> Option<usize> {
    match attr {
        "w" => Some(0),
        "x" => Some(1),
        "y" => Some(2),
        "z" => Some(3),
        _ => None,
    }
}

fn get(state: &State, attr: &str) -> i64 {
    match index_for(attr) {
        Some(i) => state[i],
        _ => attr.parse().unwrap(),
    }
}

fn put(state: &State, attr: &str, val: i64) -> State {
    let mut result = *state;
    result[index_for(attr).unwrap()] = val;
    result
}

fn eq(state: &State, attr: &str, attr2: &str) -> i64 {
    if get(state, attr) == get(state, attr2) {
        1
    } else {
        0
    }
}

fn command(
    state: &State,
    inputs: &mut Vec<i64>,
    cmd: &str,
    attr: &str,
    attr2: Option<&str>,
) -> State {
    let val1 = get(state, attr);
    let val2 = attr2.map(|a| get(state, a));

    let result = match cmd {
        "inp" => inputs.pop().unwrap(),
        "add" => val1 + val2.unwrap(),
        "mul" => val1 * val2.unwrap(),
        "div" => val1 / val2.unwrap(),
        "mod" => val1 % val2.unwrap(),
        "eql" => {
            if val1 == val2.unwrap() {
                1
            } else {
                0
            }
        }
        _ => 0,
    };

    put(state, attr, result)
}

fn run(inputs: &mut Vec<i64>, program: &str) -> State {
    let mut state = [0, 0, 0, 0];
    for l in program.lines() {
        let mut parts = l.split(' ');
        let cmd = parts.next().unwrap();
        let attr = parts.next().unwrap();
        state = command(&state, inputs, cmd, attr, parts.next());
    }
    state
}

pub fn run_searching(program: &str) -> i64 {
    let mut at_input = 0;
    let mut inputs: Vec<i64> = vec![];
    let mut states: Vec<(i64, [i64; 4])> = vec![(0, [0, 0, 0, 0])];
    for l in program.lines() {
        let mut parts = l.split(' ');
        let cmd = parts.next().unwrap();
        let attr = parts.next().unwrap();
        let attr2 = parts.next();
        println!("cmd: -{}-", cmd);
        if cmd == "inp" {
            let mut found: HashSet<(i64, i64, i64)> = HashSet::new();
            let mut result = vec![];
            for (i, [_, x, y, z]) in states
                .iter()
                .sorted_by(|(i1, _), (i2, _)| Ord::cmp(&i2, &i1))
            {
                if found.contains(&(*x, *y, *z)) {
                    continue;
                };
                found.insert((*x, *y, *z));
                for n in 1..=9 {
                    result.push((10 * i + n, [n, *x, *y, *z]));
                }
            }
            at_input += 1;
            println!(
                "At input {}: came with {}, kept {}, continued with {}",
                at_input,
                states.len(),
                found.len(),
                result.len()
            );
            states = result;
        } else {
            for (_, s) in states.iter_mut() {
                *s = command(s, &mut inputs, cmd, attr, attr2);
            }
        }
    }

    *states
        .iter()
        .filter(|(_, [_, _, _, z])| z == &0)
        .map(|(n, _)| n)
        .sorted()
        .rev()
        .next()
        .unwrap()
}

pub fn run_searching_2(program: &str) -> i64 {
    let mut at_input = 0;
    let mut inputs: Vec<i64> = vec![];
    let mut states: Vec<(i64, [i64; 4])> = vec![(0, [0, 0, 0, 0])];
    for l in program.lines() {
        let mut parts = l.split(' ');
        let cmd = parts.next().unwrap();
        let attr = parts.next().unwrap();
        let attr2 = parts.next();
        println!("cmd: -{}-", cmd);
        if cmd == "inp" {
            let mut found: HashSet<(i64, i64, i64)> = HashSet::new();
            let mut result = vec![];
            for (i, [_, x, y, z]) in states
                .iter()
                .sorted_by(|(i1, _), (i2, _)| Ord::cmp(&i1, &i2))
            {
                if found.contains(&(*x, *y, *z)) {
                    continue;
                };
                found.insert((*x, *y, *z));
                for n in 1..=9 {
                    result.push((10 * i + n, [n, *x, *y, *z]));
                }
            }
            at_input += 1;
            println!(
                "At input {}: came with {}, kept {}, continued with {}",
                at_input,
                states.len(),
                found.len(),
                result.len()
            );
            states = result;
        } else {
            for (_, s) in states.iter_mut() {
                *s = command(s, &mut inputs, cmd, attr, attr2);
            }
        }
    }

    *states
        .iter()
        .filter(|(_, [_, _, _, z])| z == &0)
        .map(|(n, _)| n)
        .sorted()
        .next()
        .unwrap()
}

pub fn part_a(input: &str) -> String {
    run_searching(input).to_string()
}

pub fn part_b(input: &str) -> String {
    run_searching_2(input).to_string()
}

#[cfg(test)]
mod tests {
    use super::super::utils;
    use super::*;

    #[test]
    fn test_put() {
        let s: State = [0, 1, 2, 3];
        assert_eq!(put(&s, "x", 7), [0, 7, 2, 3]);
    }

    #[test]
    fn test_run() {
        let program = "inp w
add z w
mod z 2
div w 2
add y w
mod y 2
div w 2
add x w
mod x 2
div w 2
mod w 2";
        let mut state = vec![9];
        assert_eq!(run(&mut state, program), [1, 0, 0, 1]);
    }

    #[test]
    fn test_run_searching() {
	println!("{}",
        run_searching(&utils::input("day_24")));
    }
}
