use regex::Regex;
use std::collections::{HashMap, HashSet};

// target area: x=20..30, y=-10..-5

fn parse(input: &str) -> (i32, i32, i32, i32) {
    let re = Regex::new(r"target area: x=(\d+)\.\.(\d+), y=(-?\d+)\.\.(-?\d+)").unwrap();
    let cap = re.captures_iter(input).next().unwrap();

    let x_from: i32 = cap[1].parse().unwrap();
    let x_to: i32 = cap[2].parse().unwrap();
    let y_from: i32 = cap[3].parse().unwrap();
    let y_to: i32 = cap[4].parse().unwrap();

    (x_from, x_to, y_from, y_to)
}

fn pos_y(vx: i32, n: i32) -> i32 {
    n * vx - n * (n - 1) / 2
}

fn pos_x(vx: i32, n: i32) -> i32 {
    if n > vx {
        return vx * (vx + 1) / 2;
    }
    pos_y(vx, n)
}

fn x_to(x1: i32, x2: i32, n_min: i32, n_max: i32) -> HashMap<i32, Vec<i32>> {
    let vx_min = (((2 * x1) as f64 + 0.25).sqrt() - 0.5).ceil() as i32;
    let mut result = HashMap::new();

    for vx in vx_min..=x2 {
        for n in n_min..=n_max {
            let x = pos_x(vx, n);
            if x > x2 {
                break;
            }
            if x1 <= x && x <= x2 {
                result.entry(vx).or_insert_with(Vec::new).push(n);
            }
        }
    }

    result
}

fn y_to(y1: i32, y2: i32) -> HashMap<i32, Vec<i32>> {
    let mut result = HashMap::new();

    for vy in y1..=-y1 {
        for n in (2 * vy + 1).. {
            let y = pos_y(vy, n);
            if y < y1 {
                break;
            }
            if y1 <= y && y <= y2 {
                result.entry(vy).or_insert_with(Vec::new).push(n);
            }
        }
    }

    result
}

pub fn run(input: &str) -> HashSet<(i32, i32)> {
    let (x1, x2, y1, y2) = parse(input);
    let ys = y_to(y1, y2);
    let n_max = ys.values().flatten().max().unwrap();
    let n_min = ys.values().flatten().min().unwrap();
    let xs = x_to(x1, x2, *n_min, *n_max);

    let mut result: HashSet<(i32, i32)> = HashSet::new();

    for (y, ns) in ys {
        for n in ns {
            for (x, ns2) in &xs {
                if ns2.contains(&n) {
                    result.insert((*x, y));
                }
            }
        }
    }

    result
}

pub fn part_a(input: &str) -> String {
    let vy = *run(input).iter().map(|(_, y)| y).max().unwrap();
    (vy * (vy + 1) / 2).to_string()
}

pub fn part_b(input: &str) -> String {
    run(input).len().to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn run() {
        let input = "target area: x=201..230, y=-99..-65";
        println!("{}", part_a(input));
        println!("{}", part_b(input));
    }

    #[test]
    fn test_parse() {
        let (x1, x2, y1, y2) = parse("target area: x=20..30, y=-10..-5");
        assert_eq!(x2 - x1, 10);
        assert_eq!(y2 - y1, 5);
    }
}
