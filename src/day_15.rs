use std::{
    cmp::Reverse,
    collections::{BinaryHeap, HashMap},
};

fn parse(input: &str) -> HashMap<(i32, i32), i32> {
    let mut result = HashMap::new();
    for (y, line) in input.lines().enumerate() {
        for (x, c) in line.chars().enumerate() {
            result.insert((x as i32, y as i32), c.to_string().parse().unwrap());
        }
    }
    result
}

fn neighbours((x, y): &(i32, i32), (width, height): (i32, i32)) -> Vec<(i32, i32)> {
    let mut result = Vec::new();

    let deltas = [(-1, 0), (0, -1), (0, 1), (1, 0)];
    for (dx, dy) in deltas {
        let x2 = x + dx;
        let y2 = y + dy;
        if x2 < 0 || x2 >= width || y2 < 0 || y2 >= height {
            continue;
        }
        result.push((x2, y2))
    }
    result
}

fn five_times(weights: HashMap<(i32, i32), i32>) -> HashMap<(i32, i32), i32> {
    let width = weights.keys().map(|(x, _)| x).max().unwrap() + 1;
    let height = weights.keys().map(|(_, y)| y).max().unwrap() + 1;

    let mut result = HashMap::new();

    for x in 0..5 * width {
        for y in 0..5 * height {
            let n_x = x / width;
            let n_y = y / height;
            let r_x = x % width;
            let r_y = y % height;

            let weight = ((weights[&(r_x, r_y)] + n_x + n_y - 1) % 9) + 1;
            result.insert((x, y), weight);
        }
    }
    result
}

fn dijkstra(weights: HashMap<(i32, i32), i32>) -> i32 {
    let width = weights.keys().map(|(x, _)| x).max().unwrap() + 1;
    let height = weights.keys().map(|(_, y)| y).max().unwrap() + 1;

    let target = (width - 1, height - 1);
    let origin = (0, 0);

    let mut costs = HashMap::from([]);

    let mut queue: BinaryHeap<(Reverse<i32>, (i32, i32))> = BinaryHeap::new();
    queue.push((Reverse(0), origin));

    while let Some((Reverse(weight), current)) = queue.pop() {
        if current == target {
            return weight;
        };

        if let Some(&cost) = costs.get(&current) {
            if cost < weight {
                continue;
            }
        }

        let ns = neighbours(&current, (width, height));

        for n in ns.iter() {
            let new_cost = weights[n] + weight;
            let cost = costs.get(n);

            if let Some(c) = cost {
                if *c <= new_cost {
                    continue;
                }
            }

            queue.push((Reverse(new_cost), *n));
            costs.insert(*n, new_cost);
        }
    }
    0
}

pub fn part_a(input: &str) -> String {
    dijkstra(parse(input)).to_string()
}

pub fn part_b(input: &str) -> String {
    dijkstra(five_times(parse(input))).to_string()
}

#[cfg(test)]
mod tests {
    use super::super::utils;
    use super::*;

    #[test]
    fn run() {
        println!("{}", part_b(&utils::input("day_15")));
    }

    const TEST_INPUT: &str = "183\n291\n304";

    #[test]
    fn test_parse() {
        let parsed: HashMap<(i32, i32), i32> = parse(TEST_INPUT);
        assert_eq!(parsed.len(), 9);
        assert_eq!(parsed[&(2, 2)], 4);
    }

    #[test]
    fn test_dijkstra() {
        let parsed: HashMap<(i32, i32), i32> = parse(TEST_INPUT);
        println!("{}", dijkstra(parsed))
    }

    const DEMO_INPUT: &str = "1163751742
1381373672
2136511328
3694931569
7463417111
1319128137
1359912421
3125421639
1293138521
2311944581";

    #[test]
    fn test_part_a() {
        assert_eq!(part_a(DEMO_INPUT), "40");
    }

    #[test]
    fn test_part_b() {
        assert_eq!(part_b(DEMO_INPUT), "315")
    }
}
