use std::collections::HashMap;
use regex::Regex;

fn range(from: i32, to: i32) -> Vec<i32> {
    if from <= to {
        (from..=to).collect()
    } else {
        (0..=from - to).map(|x| from - x).collect()
    }
}

type Line = (i32, i32, i32, i32);

fn points((x1, y1, x2, y2): Line) -> Vec<(i32, i32)> {
    if x1 == x2 {
        return range(y1, y2).iter().map(|&y| (x1, y)).collect();
    } else if y1 == y2 {
        return range(x1, x2).iter().map(|&x| (x, y1)).collect();
    };
    vec![]
}

fn points_with_diagonal((x1, y1, x2, y2): Line) -> Vec<(i32, i32)> {
    if (y2 - y1).abs() != (x2 - x1).abs() {
        return points((x1, y1, x2, y2));
    }

    range(x1, x2)
        .iter()
        .zip(range(y1, y2).iter())
        .map(|(&x, &y)| (x, y))
        .collect()
}

fn parse_line(line: &str) -> Line {
    let re = Regex::new(r"(\d+),(\d+) -> (\d+),(\d+)").unwrap();
    let c = re.captures_iter(line).next().unwrap();
    (
        c[1].parse().unwrap(),
        c[2].parse().unwrap(),
        c[3].parse().unwrap(),
        c[4].parse().unwrap(),
    )
}

fn count(data: &str, point_function: fn(Line) -> Vec<(i32, i32)>) -> usize {
    let points_in_lines = data
        .lines()
        .map(parse_line)
        .flat_map(point_function)
        .collect::<Vec<_>>();
    
    let mut counts: HashMap<(i32, i32), i32> = HashMap::new();

    for point in points_in_lines {
        *counts.entry(point).or_insert(0) += 1;
    }

    counts.values().filter(|&v| *v > 1).count()
}

pub fn part_a(input: &str) -> String {
    count(input, points).to_string()
}

pub fn part_b(input: &str) -> String {
    count(input, points_with_diagonal).to_string()
}
