use itertools::Itertools;
use std::collections::{HashMap, HashSet};

struct Inputs {
    inputs: HashSet<String>,
}

impl Inputs {
    pub fn find_and_remove(&mut self, pred: impl Fn(&&String) -> bool) -> String {
        let to_remove = self.inputs.iter().find(pred).unwrap().clone();
        self.inputs.remove(&to_remove);
        to_remove
    }
}

fn is_superset(a: &str, b: &str) -> bool {
    let a: HashSet<char> = a.chars().collect();
    let b: HashSet<char> = b.chars().collect();
    a.is_superset(&b)
}

struct Line {
    inputs: Inputs,
    outputs: Vec<String>,
    digits: HashMap<String, i32>,
}

impl Line {
    pub fn new(inputs: HashSet<String>, outputs: Vec<String>) -> Line {
        let inputs = Inputs { inputs };
        Line {
            inputs,
            outputs,
            digits: HashMap::new(),
        }
    }

    pub fn unique_outputs(&self) -> usize {
        let uniques: [usize; 4] = [2, 3, 4, 7];
        self.outputs
            .iter()
            .map(|x| x.len())
            .filter(|&x| uniques.contains(&x))
            .count()
    }

    pub fn set_digits(&mut self) {
        let one = self.inputs.find_and_remove(|s| s.len() == 2);

        let four = self.inputs.find_and_remove(|s| s.len() == 4);
        let seven = self.inputs.find_and_remove(|s| s.len() == 3);
        let eight = self.inputs.find_and_remove(|s| s.len() == 7);
        let nine = self
            .inputs
            .find_and_remove(|s| s.len() == 6 && is_superset(s, &four));
        let zero = self
            .inputs
            .find_and_remove(|s| s.len() == 6 && is_superset(s, &seven));
        let six = self.inputs.find_and_remove(|s| s.len() == 6);
        let three = self
            .inputs
            .find_and_remove(|s| s.len() == 5 && is_superset(s, &one));
        let five = self
            .inputs
            .find_and_remove(|s| s.len() == 5 && is_superset(&nine, s));
        let two = self.inputs.find_and_remove(|_| true);

        self.digits.insert(zero, 0);
        self.digits.insert(one, 1);
        self.digits.insert(two, 2);
        self.digits.insert(three, 3);
        self.digits.insert(four, 4);
        self.digits.insert(five, 5);
        self.digits.insert(six, 6);
        self.digits.insert(seven, 7);
        self.digits.insert(eight, 8);
        self.digits.insert(nine, 9);
    }

    pub fn get_result(&self) -> i32 {
        self.outputs
            .iter()
            .map(|s| self.digits.get(s).unwrap().to_string())
            .collect::<String>()
            .parse()
            .unwrap()
    }
}

fn parse(input: &str) -> Line {
    let parts = input.split('|').collect::<Vec<_>>();
    let outputs = parts[1]
        .trim()
        .split_whitespace()
        .map(|s| s.chars().sorted().collect())
        .collect();
    let inputs = parts[0]
        .trim()
        .split_whitespace()
        .map(|s| s.chars().sorted().collect())
        .collect();
    Line::new(inputs, outputs)
}

pub fn part_a(input: &str) -> String {
    let result: usize = input.lines().map(|i| parse(i).unique_outputs()).sum();

    result.to_string()
}

pub fn part_b(input: &str) -> String {
    let result: i32 = input
        .lines()
        .map(parse)
        .map(|mut l| {
            l.set_digits();
            l.get_result()
        })
        .sum();

    result.to_string()
}

#[cfg(test)]
mod tests {
    use super::{super::utils, *};

    #[test]
    fn run() {
        println!("{}", part_a(&utils::input("day_08_demo")));
        println!("{}", part_a(&utils::input("day_08")));
        println!("{}", part_b(&utils::input("day_08_demo")));
        println!("{}", part_b(&utils::input("day_08")));
    }

    #[test]
    fn test_parses_outputs() {
        let input = "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe";
        assert_eq!(parse(input).outputs.len(), 4);
    }

    #[test]
    fn test_find_output_lengths() {
        let line = Line::new(
            HashSet::new(),
            vec!["a", "ab", "abc", "abcd", "afbcde"]
                .iter()
                .map(|s| s.to_string())
                .collect(),
        );
        assert_eq!(line.unique_outputs(), 3);
    }

    #[test]
    fn test_set_digits() {
        let input = "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe";
        let mut line = parse(input);
        line.set_digits();

        assert_eq!(line.inputs.inputs.len(), 0);

        assert_eq!(line.digits.get("bceg").unwrap(), &4);
    }

    #[test]
    fn test_get_result() {
        let mut digits = HashMap::new();
        digits.insert("a".to_string(), 1);
        digits.insert("b".to_string(), 2);
        let inputs = Inputs {
            inputs: HashSet::new(),
        };
        let outputs = vec!["a", "b", "a", "b"]
            .iter()
            .map(|s| s.to_string())
            .collect();
        let line = Line {
            inputs,
            outputs,
            digits,
        };
        assert_eq!(line.get_result(), 1212);
    }

    #[test]
    fn test_integration() {
        let input =
            "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf";
        let mut line = parse(input);
        line.set_digits();
        assert_eq!(line.get_result(), 5353);
    }
}
