use std::collections::{HashMap, HashSet};

fn parse(input: &str) -> HashMap<(i32, i32), i32> {
    let mut result = HashMap::new();
    for (y, line) in input.lines().enumerate() {
        for (x, i) in line
            .chars()
            .map(|x| x.to_string().parse::<i32>().unwrap())
            .enumerate()
        {
            result.insert((x as i32, y as i32), i);
        }
    }
    result
}

fn neighbours(pos: &(i32, i32), width: i32, height: i32) -> Vec<(i32, i32)> {
    let deltas = vec![
        (-1, -1),
        (-1, 0),
        (-1, 1),
        (0, -1),
        (0, 1),
        (1, -1),
        (1, 0),
        (1, 1),
    ];

    deltas
        .iter()
        .map(|(x, y)| (pos.0 + x, pos.1 + y))
        .filter(|&(x, y)| x >= 0 && y >= 0 && x < width && y < height)
        .collect()
}

fn step(field: &mut HashMap<(i32, i32), i32>) -> i32 {
    let mut queue: Vec<_> = field.keys().cloned().collect();
    let height = queue.iter().filter(|&&(x, _)| x == 0).count() as i32;
    let width = queue.iter().filter(|&&(_, y)| y == 0).count() as i32;

    let mut flashed: HashSet<(i32, i32)> = HashSet::new();

    while !queue.is_empty() {
        let p = queue.pop().unwrap();
        if flashed.contains(&p) {
            continue;
        }

        let octopus = field.entry(p).or_default();
        *octopus += 1;

        if octopus == &10 {
            *octopus = 0;
            flashed.insert(p);

            queue.extend(neighbours(&p, width, height));
        }
    }

    flashed.len() as i32
}

fn steps(field: &mut HashMap<(i32, i32), i32>, n: i32) -> i32 {
    let mut flashed = 0;
    for _ in 1..=n {
        flashed += step(field);
    }
    flashed
}

pub fn part_a(input: &str) -> String {
    let mut field = parse(input);
    steps(&mut field, 100).to_string()
}

pub fn part_b(input: &str) -> String {
    let mut field = parse(input);
    let mut last = 0;
    let mut n = 0;
    while last < 100 {
        last = step(&mut field);
        n += 1;
    }
    n.to_string()
}

#[cfg(test)]
mod tests {
    use super::super::utils;
    use super::*;

    const TEST_FIELD: &str = "123\n456\n589";
    const DEMO_FIELD: &str = "5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526";

    #[test]
    fn run() {
        println!("{}", part_a(&utils::input("day_11")));
        println!("{}", part_b(&utils::input("day_11")));
    }

    #[test]
    fn parses() {
        let field = parse(TEST_FIELD);
        assert_eq!(field.len(), 9);
    }

    #[test]
    fn test_step() {
        let mut field = parse(TEST_FIELD);
        let banged = step(&mut field);
        assert_eq!(field[&(0, 0)], 2);

        assert_eq!(field[&(2, 2)], 0);
        assert_eq!(field[&(1, 2)], 0);

        assert_eq!(field[&(1, 1)], 8);
        assert_eq!(banged, 2);
    }

    #[test]
    fn test_demo() {
        let mut field = parse(DEMO_FIELD);
        let banged = step(&mut field);
        assert_eq!(banged, 0);

        let banged = step(&mut field);
        assert_eq!(banged, 35);

        let mut field = parse(DEMO_FIELD);
        assert_eq!(steps(&mut field, 10), 204);
    }

    #[test]
    fn test_part_a() {
        assert_eq!(part_a(DEMO_FIELD), "1656");
    }

    #[test]
    fn test_part_b() {
        assert_eq!(part_b(DEMO_FIELD), "195");
    }
}
