use std::collections::HashSet;

type Point = (usize, usize);
type State = (HashSet<Point>, HashSet<Point>, Point);

fn parse(input: &str) -> State {
    let mut south = HashSet::new();
    let mut east = HashSet::new();
    let mut point = (0, 0);
    for (y, line) in input.lines().enumerate() {
        for (x, c) in line.chars().enumerate() {
            point = (x, y);
            match c {
                'v' => {
                    south.insert(point);
                }
                '>' => {
                    east.insert(point);
                }
                _ => {}
            }
        }
    }
    (south, east, point)
}

fn step((south, east, size): &State) -> (State, i64) {
    let mut moved = 0;
    let mut new_east = HashSet::new();
    for &(x, y) in east {
        let mut new = (x + 1, y);
        if new.0 > size.0 {
            new.0 = 0
        }
        if east.contains(&new) || south.contains(&new) {
            new_east.insert((x, y));
        } else {
            moved += 1;
            new_east.insert(new);
        }
    }
    let mut new_south = HashSet::new();
    for &(x, y) in south {
        let mut new = (x, y + 1);
        if new.1 > size.1 {
            new.1 = 0
        }
        if new_east.contains(&new) || south.contains(&new) {
            new_south.insert((x, y));
        } else {
            moved += 1;
            new_south.insert(new);
        }
    }
    ((new_south, new_east, *size), moved)
}

pub fn part_a(input: &str) -> String{
    let mut state = parse(input);
    let mut steps = 0;
    loop {
        let (state2, moved) = step(&state);
        state = state2;
        steps += 1;
        println!("{}, {}", steps, moved);
        if moved == 0 {
            break;
        }
    }
    steps.to_string()
}

#[cfg(test)]
mod tests {
    use super::super::utils;
    use super::*;

    #[test]
    fn run() {
	println!("{}", part_a(&utils::input("day_25")));
    }

    const DEMO_INPUT: &str = "v...>>.vv>
.vv>>.vv..
>>.>v>...v
>>v>>.>.v.
v>v.vv.v..
>.>>..v...
.vv..>.>v.
v.v..>>v.v
....v..v.>";

    #[test]
    fn test_parse() {
        let (south, east, size) = parse(DEMO_INPUT);
        assert!(south.contains(&(0, 0)));
        assert!(east.contains(&(5, 2)));
        assert!(!south.contains(&(3, 4)));
        assert!(!east.contains(&(3, 4)));
    }

    #[test]
    fn test_step() {
        let mut state = parse(">>..>");
        println!("{:?}", state);
        state = step(&state).0;
        println!("{:?}", state);
        state = step(&state).0;
        println!("{:?}", state);
        state = step(&state).0;
        println!("{:?}", state);
        state = step(&state).0;
        println!("{:?}", state);
    }

    #[test]
    fn test_part_a() {
	println!("{}", part_a(DEMO_INPUT));
    }
}
