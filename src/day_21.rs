use std::{collections::{HashMap, btree_map::OccupiedEntry, hash_map::Entry}, hash::Hash};

#[derive(Debug)]
struct State {
    player_a: (i64, i64),
    player_b: (i64, i64),
    next_roll: i64,
    rolled: i64,
}

impl State {
    fn new(scores: (i64, i64)) -> Self {
        Self {
            player_a: (scores.0, 0),
            player_b: (scores.1, 0),
            next_roll: 1,
            rolled: 0,
        }
    }
}

fn step(state: &State) -> State {
    let (pos, points) = state.player_a;
    let new_pos = (state.next_roll * 3 + 3 + pos - 1) % 10 + 1;

    State {
        player_a: state.player_b,
        player_b: (new_pos, points + new_pos),
        next_roll: (state.next_roll + 3) % 100,
        rolled: state.rolled + 3,
    }
}

fn part_a(scores: (i64, i64)) {
    let mut state = State::new(scores);

    while state.player_b.1 < 1000 {
        state = step(&state);
    }

    println!("{}", state.player_a.1 * state.rolled);
}

#[derive(Debug, Hash, PartialEq, Eq, Clone)]
struct StateDirac {
    player_a: (i64, i64),
    player_b: (i64, i64),
}

impl StateDirac {
    fn new(a: i64, b: i64) -> Self {
        Self {
            player_a: (a, 0),
            player_b: (b, 0),
        }
    }

    fn wins(&self) -> bool {
        self.player_b.1 >= 21
    }
}

fn states_after(state: &StateDirac) -> HashMap<StateDirac, i64> {
    let (pos, points) = state.player_a;
    let mut result = HashMap::new();
    for roll1 in 1..=3 {
        for roll2 in 1..=3 {
            for roll3 in 1..=3 {
                let new_pos = ((pos + roll1 + roll2 + roll3 - 1) % 10) + 1;
                let new_state = StateDirac {
                    player_a: state.player_b,
                    player_b: (new_pos, points + new_pos),
                };

                let e = result.entry(new_state).or_insert(0);
                *e += 1;
            }
        }
    }

    result
}

fn wins_after(state: &StateDirac) -> (i64, i64) {
    if state.wins() {
        return (0, 1);
    }

    states_after(state)
        .iter()
        .map(|(s, &count)| {
            let (w, l) = wins_after(s);
            (w * count, l * count)
        })
        .fold((0, 0), |(wins, loses), (w, l)| (wins + l, loses + w))
}

struct Calculator {
    cache: HashMap<StateDirac, (i64, i64)>,
}

impl Calculator {
    fn new() -> Self {
	Self { cache: HashMap::new() }
    }
    
    fn wins(&mut self, state: &StateDirac) -> (i64, i64) {
        if state.wins() {
            return (0, 1);
        }

        states_after(state)
            .iter()
            .map(|(s, &count)| {

		let mut w: i64 = 0;
		let mut l: i64 = 0;

//		println!("HI!");

		let entry = self.cache.entry(s.clone());
		if let Entry::Occupied(es) = entry {
		    let r = *es.get();
		    w = r.0;
		    l = r.1;
		} else {
		    let wins = self.wins(s);
		    w = wins.0;
		    l = wins.1;
		    self.cache.insert(s.clone(), (w,l));
/*
		    let len = self.cache.len();
		    if len % 1000 == 0 {
			println!("-- {:?}", self.cache.len());
		    }
*/
		}

//		println!("{:?} {} {} {}", (count * w, count * l), count, w, l);
		(count * w, count * l)
            })
            .fold((0, 0), |(wins, loses), (w, l)| (wins + l, loses + w))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn run() {
        let s = StateDirac::new(5, 8);
        println!("{:?}", states_after(&s));

        let s = StateDirac {
            player_a: (5, 0),
            player_b: (8, 0),
        };

        //	    let mut wins_cache = HashMap::new();

        println!("{:?}", Calculator::new().wins(&s));

        /*
        let mut state = State::new();
        state = step(&state);
        state = step(&state);
        state = step(&state);

        println!("{:?}", state);
           */

        //        part_a((5,8));
    }
}
