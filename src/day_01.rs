fn increasing(v: &[i32]) -> usize {
    v.windows(2).filter(|x| x[0] < x[1]).count()
}

pub fn part_a(input: &str) -> String {
    let numbers: Vec<i32> = input.lines().map(|l| l.parse().unwrap()).collect();
    increasing(&numbers).to_string()
}

pub fn part_b(input: &str) -> String {
    let numbers: Vec<i32> = input.lines().map(|l| l.parse().unwrap()).collect();
    let triplets: Vec<i32> = numbers.windows(3).map(|x| x[0] + x[1] + x[2]).collect();
    increasing(&triplets).to_string()
}
