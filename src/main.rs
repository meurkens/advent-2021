use std::time::Instant;

mod day_01;
mod day_02;
mod day_03;
mod day_04;
mod day_05;
mod day_06;
mod day_07;
mod day_08;
mod day_09;
mod day_10;
mod day_11;
mod day_12;
mod day_13;
mod day_14;
mod day_15;
mod day_16;
mod day_17;
mod day_18;
mod day_19;
mod day_20;
mod day_21;
mod day_22;
mod day_23;
mod day_24;
mod day_25;
mod utils;

fn run(day: i32, title: &str, part_a: fn(&str) -> String, part_b: fn(&str) -> String) {
    let filename = format!("day_{:02}", day);
    let input = utils::input(&filename);
    println!("-- Day {}: {}", day, title);

    let start_a = Instant::now();
    let result_a = part_a(&input);
    println!("part a: {} (took {:.2?})", result_a, start_a.elapsed());

    let start_b = Instant::now();
    let result_b = part_b(&input);
    println!("part b: {} (took {:.2?})", result_b, start_b.elapsed());
}

fn main() {
    let started = Instant::now();
    run(1, "Sonar Sweep", day_01::part_a, day_01::part_b);
    run(2, "Dive!", day_02::part_a, day_02::part_b);
    run(3, "Binary Diagnostic", day_03::part_a, day_03::part_b);
    run(4, "Giant Squid", day_04::part_a, day_04::part_b);
    run(5, "Hydrothermal Venture", day_05::part_a, day_05::part_b);
    run(6, "Lanternfish", day_06::part_a, day_06::part_b);
    run(7, "The Treachery of Whales", day_07::part_a, day_07::part_b);
    run(8, "Seven Segment Search", day_08::part_a, day_08::part_b);
    run(9, "Smoke Bassin", day_09::part_a, day_09::part_b);
    run(10, "Syntax Scoring", day_10::part_a, day_10::part_b);
    run(11, "Dumbo Octopus", day_11::part_a, day_11::part_b);
    run(12, "Passage Pathing", day_12::part_a, day_12::part_b);
    run(13, "Transparent Origami", day_13::part_a, day_13::part_b);
    run(14, "Extended Polymerization", day_14::part_a, day_14::part_b);
    run(15, "Chiton", day_15::part_a, day_15::part_b);
    run(16, "Packet Decoder", day_16::part_a, day_16::part_b);
    run(17, "Trick Shot", day_17::part_a, day_17::part_b);
    run(18, "Snailfish", day_18::part_a, day_18::part_b);
    run(20, "Trench Map", day_20::part_a, day_20::part_b);
    run(22, "", day_22::part_a, day_22::part_b);
    run(23, "", day_23::part_a, day_23::part_b);
    // run(24, "", day_24::part_a, day_24::part_b);
    println!("\nTotal runtime: {:.2?}", started.elapsed());
}
