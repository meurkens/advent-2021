use std::{
    cmp::Ordering,
    collections::{BinaryHeap, HashMap, HashSet},
};

use itertools::Itertools;

type Point = (usize, usize);
type Positions = HashMap<Point, char>;

#[derive(PartialEq, Eq)]
struct State {
    cost: i32,
    positions: Positions,
}

impl Ord for State {
    fn cmp(&self, other: &Self) -> Ordering {
        other.cost.cmp(&self.cost)
    }
}

impl PartialOrd for State {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

fn parse(input: &str) -> Positions {
    let mut pieces: Positions = HashMap::new();

    for (y, line) in input.lines().enumerate() {
        for (x, c) in line.chars().enumerate() {
            if HashSet::from(['A', 'B', 'C', 'D']).contains(&c) {
                pieces.insert((x, y), c);
            }
        }
    }

    pieces
}

fn diff(l: usize, r: usize) -> usize {
    if r > l {
        r - l
    } else {
        l - r
    }
}

fn mov(positions: &Positions, from: Point, to: Point) -> Positions {
    let mut new_positions = positions.to_owned();
    let c = new_positions.remove(&from).unwrap();
    new_positions.insert(to, c);
    new_positions
}

fn is_complete(positions: &Positions) -> bool {
    positions.get(&(3, 2)) == Some(&'A')
        && positions.get(&(5, 2)) == Some(&'B')
        && positions.get(&(7, 2)) == Some(&'C')
        && positions.get(&(9, 2)) == Some(&'D')
}

fn fingerprint(positions: &Positions) -> String {
    positions
        .iter()
        .map(|(k, _)| k)
        .sorted()
        .map(|k| format!("{:?}{:?}", k, positions[k]))
        .join("")
}

fn x_for(c: char) -> usize {
    match c {
        'A' => 3,
        'B' => 5,
        'C' => 7,
        'D' => 9,
        _ => panic!("wut"),
    }
}

fn multiplier_for(c: char) -> usize {
    match c {
        'A' => 1,
        'B' => 10,
        'C' => 100,
        'D' => 1000,
        _ => panic!("wut"),
    }
}

fn steps(positions: &Positions, max_y: usize) -> Vec<(Positions, i32)> {
    let taken_positions: HashSet<Point> = positions.iter().map(|(p, _)| *p).collect();
    let mut result = Vec::new();

    'to: for (&(x, y), &c) in positions.iter() {
        let multiplier = multiplier_for(c);
        if y > 1 {
            if taken_positions.contains(&(x, y - 1)) {
                continue;
            }

            for x_new in [1, 2, 4, 6, 8, 10, 11] {
                let range = if x_new < x { x_new..=x } else { x..=x_new };
                let range = range.map(|x_| (x_, 1)).collect::<HashSet<_>>();
                if range.is_disjoint(&taken_positions) {
                    result.push((
                        mov(positions, (x, y), (x_new, 1)),
                        ((y - 1 + diff(x_new, x)) * multiplier) as i32,
                    ));
                }
            }
        }

        if y == 1 {
            let x_new = x_for(c);
            let mut y_new = max_y;

            while let Some(&c_taken) = positions.get(&(x_new, y_new)) {
                if c_taken == c {
                    y_new -= 1;
                } else {
                    continue 'to;
                }
            }

            let range = if x_new < x {
                x_new..=(x - 1)
            } else {
                (x + 1)..=x_new
            };
            let range = range.map(|x_| (x_, 1)).collect::<HashSet<_>>();
            if range.is_disjoint(&taken_positions) {
                result.push((
                    mov(positions, (x, y), (x_new, y_new)),
                    ((y_new - 1 + diff(x_new, x)) * multiplier) as i32,
                ));
            }
        }
    }

    result
}

pub fn part_a(input: &str) -> String {
    let positions = parse(input);
    let max_y = positions.keys().map(|(_, y)| *y).max().unwrap();

    let mut result: HashMap<String, i32> = HashMap::new();
    let mut queue = BinaryHeap::new();

    queue.push(State { cost: 0, positions });

    while let Some(State { cost, positions }) = queue.pop() {
        if is_complete(&positions) {
            return cost.to_string();
        }

        let fp = fingerprint(&positions);

        if let Some(&current) = result.get(&fp) {
            if current <= cost {
                continue;
            }
        }

        result.insert(fp, cost);

        for (ps, added) in steps(&positions, max_y) {
            let next = State {
                positions: ps,
                cost: added + cost,
            };
            queue.push(next);
        }
    }

    panic!("Should never get here");
}

pub fn part_b(input: &str) -> String {
    let inbetween = "
  #D#C#B#A#
  #D#B#A#C#
";

    let input = format!(
        "{}{}{}",
        input.lines().take(3).join("\n"),
        inbetween,
        input.lines().skip(3).join("\n")
    );

    println!("{}", input);

    part_a(&input)
}

#[cfg(test)]
mod tests {
    use super::super::utils;
    use super::*;

    const DEMO_INPUT: &str = "#############
#...........#
###B#C#B#D###
  #A#D#C#A#
  #########";

    const DEMO_INPUT_2: &str = "#############
#...........#
###B#C#B#D###
  #D#C#B#A#
  #D#B#A#C#
  #A#D#C#A#
  #########
";

    #[test]
    fn run() {
        println!("{}", part_a(&DEMO_INPUT_2));
    }

    #[test]
    fn run2() {
        println!("{}", part_b(&utils::input("day_23")));
    }
}
