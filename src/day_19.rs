use std::collections::{HashMap, HashSet};

use itertools::Itertools;
use regex::Regex;

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
struct Point(i32, i32, i32);

fn parse_scanner(input: &str) -> (i32, HashSet<Point>) {
    let mut input = input.lines();

    let scanner: i32 = {
        let first = input.next().unwrap();
        let re = Regex::new(r"--- scanner (\d+) ---").unwrap();
        let captures = re.captures(first).unwrap();
        captures[1].parse().unwrap()
    };

    let beacons = input
        .map(|l| {
            let numbers: Vec<_> = l.split(',').map(|l| l.parse::<i32>().unwrap()).collect();
            Point(numbers[0], numbers[1], numbers[2])
        })
        .collect();

    (scanner, beacons)
}

fn parse(input: &str) -> Vec<HashSet<Point>> {
    input
        .split("\n\n")
        .map(|string| {
            let (_, beacons) = parse_scanner(string);
            beacons
        })
        .collect()
}

fn coord(p: &Point, &n: &i32) -> i32 {
    if n < 0 {
        return -coord(p, &-n);
    }

    match n {
        1 => p.0,
        2 => p.1,
        3 => p.2,
        _ => {
            panic!("illegal input")
        }
    }
}

#[derive(Debug, Clone)]
enum Transformation {
    Rotation(i32, i32, i32),
    Addition(i32, i32, i32),
}

impl Transformation {
    fn apply(&self, point: &Point) -> Point {
        match self {
            Self::Rotation(x, y, z) => Point(coord(point, x), coord(point, y), coord(point, z)),
            Self::Addition(x, y, z) => Point(point.0 + x, point.1 + y, point.2 + z),
        }
    }

    fn add(&self, other: &Self) -> Self {
        if let Self::Addition(x, y, z) = self {
            if let Self::Addition(x1, y1, z1) = other {
                Self::Addition(x + x1, y + y1, z + z1)
            } else {
                panic!();
            }
        } else {
            panic!();
        }
    }
}

fn try_rotation(
    source: &HashSet<Point>,
    to_match: &HashSet<Point>,
    rotations: (&Transformation, &Transformation),
) -> Option<Transformation> {
    let to_match = to_match
        .iter()
        .map(|p| rotations.1.apply(&rotations.0.apply(p)));

    let to_try_from_matching = to_match.clone().skip(11);
    let to_try_from_source = source.iter().skip(11);
    let to_try_combinations = to_try_from_matching.cartesian_product(to_try_from_source);

    for (from_match, from_source) in to_try_combinations {
        let diff = Transformation::Addition(
            from_source.0 - from_match.0,
            from_source.1 - from_match.1,
            from_source.2 - from_match.2,
        );

        let to_match: HashSet<_> = to_match
            .clone()
            .map(|p| diff.apply(&p))
            .filter(|&Point(x, y, z)| {
                x >= -1000 && x <= 1000 && y >= -1000 && y <= 1000 && z >= -1000 && z <= 1000
            })
            .collect();

        if to_match.len() < 12 {
            continue;
        }

        if source.is_superset(&to_match) {
            return Some(diff);
        }
    }

    None
}

const ROTATIONS_2D: [Transformation; 4] = [
    Transformation::Rotation(1, 2, 3),
    Transformation::Rotation(-2, 1, 3),
    Transformation::Rotation(-1, -2, 3),
    Transformation::Rotation(2, -1, 3),
];

const ROTATIONS_3D: [Transformation; 6] = [
    Transformation::Rotation(1, 2, 3),
    Transformation::Rotation(3, 2, -1),
    Transformation::Rotation(-1, 2, -3),
    Transformation::Rotation(-3, 2, 1),
    Transformation::Rotation(1, 3, -2),
    Transformation::Rotation(1, -3, 2),
];

/*

let turns = [(x, y, z), (-y, x, z), (-x, -y, z), (y, -x, z)]

let points = [(x, y, z), (z, y, -x),
(-x, y, z), (-z, y, x),
(x, z, -y), (x, -z, y)]

 */

fn find_transformation(
    source: &HashSet<Point>,
    matching: &HashSet<Point>,
) -> Option<(Transformation, Transformation, Transformation)> {
    for rot_2d in ROTATIONS_2D {
        for rot_3d in ROTATIONS_3D {
            if let Some(add) = try_rotation(source, matching, (&rot_2d, &rot_3d)) {
                return Some((rot_2d, rot_3d, add));
            }
        }
    }
    None
}

fn run(scanners: &[HashSet<Point>]) -> i32 {
    let mut scanners = scanners.to_owned();
    let mut found = HashMap::from([(0_usize, Transformation::Addition(0, 0, 0))]);

    while found.len() < scanners.len() {
        println!("{:?}", scanners[1].iter().next());
        //	println!("found: {}, scanners: {}", found.len(),scanners.len());
        let scanners2 = scanners.clone();
        for (i, beacons) in scanners.iter_mut().enumerate() {
            if found.contains_key(&i) {
                continue;
            }
            let mut to_add = None;
            for (&j, found_transformation) in found.iter() {
                //		println!("j: {}", j);
                if let Some((rot_2d, rot_3d, add)) = find_transformation(&scanners2[j], beacons) {
                    //		    println!("found!");
                    let beacons_mutated: HashSet<_> = beacons
                        .iter()
                        .map(|b| rot_3d.apply(&rot_2d.apply(b)))
                        .collect();
                    *beacons = beacons_mutated;
                    to_add = Some((i, add.add(found_transformation)));
                    println!("FOUND: {}, at: {:?}", i, add.add(found_transformation));
                    break;
                }
            }
            if let Some((i, add)) = to_add {
                found.insert(i, add);
                break;
            }
        }
    }

    let scanner_positions = found.values().cloned().collect::<Vec<_>>();

    let mut result = HashSet::new();
    for (i, transformation) in found {
        for beacon in scanners[i].iter() {
            let b = transformation.apply(beacon);
            result.insert(b);
        }
    }

    println!("{}", result.len());

    
    scanner_positions.iter()
        .cartesian_product(scanner_positions.iter())
        .map(|(t1, t2)| (t1.apply(&Point(0,0,0)), t2.apply(&Point(0,0,0))))
        .map(|(p1, p2)| (p1.0 - p2.0).abs() + (p1.1 - p2.1).abs() + (p1.2 - p2.2).abs())
        .max()
        .unwrap()
}

#[cfg(test)]
mod tests {
    use super::super::utils;
    use super::*;

    #[test]
    fn test_run() {
        let input = utils::input("day_19");
        println!("{}", run(&parse(&input)));
    }

    #[test]
    fn test_something() {
        let input = "--- scanner 0 ---
404,-588,-901
528,-643,409
-838,591,734
390,-675,-793
-537,-823,-458
-485,-357,347
-345,-311,381
-661,-816,-575
-876,649,763
-618,-824,-621
553,345,-567
474,580,667
-447,-329,318
-584,868,-557
544,-627,-890
564,392,-477
455,729,728
-892,524,684
-689,845,-530
423,-701,434
7,-33,-71
630,319,-379
443,580,662
-789,900,-551
459,-707,401";
        let (_, beacons1) = parse_scanner(input);

        let input2 = "--- scanner 1 ---
686,422,578
605,423,415
515,917,-361
-336,658,858
95,138,22
-476,619,847
-340,-569,-846
567,-361,727
-460,603,-452
669,-402,600
729,430,532
-500,-761,534
-322,571,750
-466,-666,-811
-429,-592,574
-355,545,-477
703,-491,-529
-328,-685,520
413,935,-424
-391,539,-444
586,-435,557
-364,-763,-893
807,-499,-711
755,-354,-619
553,889,-390";

        let (_, beacons2) = parse_scanner(input2);

        println!("{:?}", run(&vec![beacons1, beacons2]));
    }

    #[test]
    fn test_part_a() {
        let demo_input = "--- scanner 0 ---
404,-588,-901
528,-643,409
-838,591,734
390,-675,-793
-537,-823,-458
-485,-357,347
-345,-311,381
-661,-816,-575
-876,649,763
-618,-824,-621
553,345,-567
474,580,667
-447,-329,318
-584,868,-557
544,-627,-890
564,392,-477
455,729,728
-892,524,684
-689,845,-530
423,-701,434
7,-33,-71
630,319,-379
443,580,662
-789,900,-551
459,-707,401

--- scanner 1 ---
686,422,578
605,423,415
515,917,-361
-336,658,858
95,138,22
-476,619,847
-340,-569,-846
567,-361,727
-460,603,-452
669,-402,600
729,430,532
-500,-761,534
-322,571,750
-466,-666,-811
-429,-592,574
-355,545,-477
703,-491,-529
-328,-685,520
413,935,-424
-391,539,-444
586,-435,557
-364,-763,-893
807,-499,-711
755,-354,-619
553,889,-390

--- scanner 2 ---
649,640,665
682,-795,504
-784,533,-524
-644,584,-595
-588,-843,648
-30,6,44
-674,560,763
500,723,-460
609,671,-379
-555,-800,653
-675,-892,-343
697,-426,-610
578,704,681
493,664,-388
-671,-858,530
-667,343,800
571,-461,-707
-138,-166,112
-889,563,-600
646,-828,498
640,759,510
-630,509,768
-681,-892,-333
673,-379,-804
-742,-814,-386
577,-820,562

--- scanner 3 ---
-589,542,597
605,-692,669
-500,565,-823
-660,373,557
-458,-679,-417
-488,449,543
-626,468,-788
338,-750,-386
528,-832,-391
562,-778,733
-938,-730,414
543,643,-506
-524,371,-870
407,773,750
-104,29,83
378,-903,-323
-778,-728,485
426,699,580
-438,-605,-362
-469,-447,-387
509,732,623
647,635,-688
-868,-804,481
614,-800,639
595,780,-596

--- scanner 4 ---
727,592,562
-293,-554,779
441,611,-461
-714,465,-776
-743,427,-804
-660,-479,-426
832,-632,460
927,-485,-438
408,393,-506
466,436,-512
110,16,151
-258,-428,682
-393,719,612
-211,-452,876
808,-476,-593
-575,615,604
-485,667,467
-680,325,-822
-627,-443,-432
872,-547,-609
833,512,582
807,604,487
839,-516,451
891,-625,532
-652,-548,-490
30,-46,-14";
        let beacons = parse(demo_input);

        println!("{}", run(&beacons));
    }
}
