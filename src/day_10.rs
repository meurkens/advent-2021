use itertools::Itertools;
use std::collections::HashMap;

pub fn corrupt(input: &str) -> i32 {
    let closings = HashMap::from([('}', '{'), ('>', '<'), (']', '['), (')', '(')]);
    let points = HashMap::from([(')', 3), (']', 57), ('}', 1197), ('>', 25137)]);

    let mut stack = "".to_string();
    for c in input.chars() {
        if closings.keys().contains(&c) {
            if stack.chars().last().unwrap_or('x') == closings[&c] {
                stack.pop();
            } else {
                return points[&c];
            }
        } else {
            stack.push(c)
        }
    }
    0
}

pub fn incomplete(input: &str) -> i64 {
    if corrupt(input) > 0 {
        return 0;
    };

    let mut closings: HashMap<char, char> = HashMap::new();
    closings.insert('{', '}');
    closings.insert('<', '>');
    closings.insert('[', ']');
    closings.insert('(', ')');

    let points = HashMap::from([(')', 1), (']', 2), ('}', 3), ('>', 4)]);

    let mut stack = "".to_string();
    for c in input.chars() {
        if points.keys().contains(&c) {
            stack.pop();
        } else {
            stack.push(c)
        }
    }

    let finisher = stack.chars().rev().map(|c| closings[&c]);
    let mut result: i64 = 0;

    for c in finisher {
        result *= 5;
        result += points[&c] as i64;
    }

    result
}

pub fn part_a(input: &str) -> String {
    let result: i32 = input.lines().map(|l| corrupt(l)).sum();

    result.to_string()
}

pub fn part_b(input: &str) -> String {
    let scores = input
        .lines()
        .map(|l| incomplete(l))
        .filter(|x| x > &0)
        .sorted()
        .collect::<Vec<_>>();

    let n = scores.len() / 2;

    scores[n].to_string()
}

#[cfg(test)]
mod tests {
    use super::super::utils;
    use super::*;

    fn test_data() -> String {
        "[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]"
            .to_string()
    }

    #[test]
    fn run() {
        part_a(&utils::input("day_10"));
        part_b(&utils::input("day_10"));
    }

    #[test]
    fn test_points() {
        assert_eq!(corrupt("([])"), 0);
        assert_eq!(corrupt("[{[{({}]{}}([{[{{{}}([]"), 57);
        assert_eq!(part_a(&test_data()), "26397");
    }

    #[test]
    fn test_incomplete() {
        assert_eq!(incomplete("[({(<(())[]>[[{[]{<()<>>"), 288957);
        assert_eq!(part_b(&test_data()), "288957");
    }
}
