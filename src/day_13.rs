use std::{collections::HashSet, cmp};

type ParseResult = (HashSet<(i32,i32)>, Vec<(char, i32)>);

fn parse(input: &str) -> ParseResult {
    let (points_input, rules_input) = input.split_once("\n\n").unwrap();
    let mut points = HashSet::new();
    for l in points_input.lines() {
	let (x,y) = l.split_once(',').unwrap();
	points.insert((x.parse::<i32>().unwrap(),
		       y.parse::<i32>().unwrap()));
    }
    let mut folds = Vec::new();
    for l in rules_input.lines() {
	let direction = l.chars().nth(11).unwrap();
	let n = l[13..].parse::<i32>().unwrap();
	folds.push((direction, n));
    }
    (points, folds)
}

fn apply(points: HashSet<(i32, i32)>, rule: (char, i32)) -> HashSet<(i32, i32)> {
    let (direction, n) = rule;
    points.into_iter()
        .map(|(x,y)| if direction == 'y' {
	    (x, cmp::min(y, 2*n - y))
	} else {
	    (cmp::min(x, 2*n - x), y)
	})
        .collect()
}

 fn run(points: HashSet<(i32, i32)>, rules: Vec<(char, i32)>) -> HashSet<(i32,i32)> {
    rules.iter().fold(points, |ps, r| apply(ps, *r))
 }

pub fn part_a(input: &str) -> String {
    let (points, rules) = parse(input);
    apply(points, rules[0]).len().to_string()
}

pub fn part_b(input: &str) -> String {
    let (points, rules) = parse(input);
    let points = run(points, rules);
    let y_max = points.iter().map(|p| p.1).max().unwrap();
    let x_max = points.iter().map(|p| p.0).max().unwrap();
    let mut result = "\n".to_string();
    for y in 0..=y_max {
	for x in 0..=x_max {
	    if points.contains(&(x,y)) {
		result += "#";
	    } else {
		result += " ";
	    }
	}
	result += "\n";
    }
    result
}

#[cfg(test)]
mod tests {
    use super::*;
    use super::super::utils;

    #[test]
    fn run() {
	let input = utils::input("day_13");
	println!("{}", part_a(&input));
	println!("{}", part_b(&input));
    }
    
    #[test]
    fn test_parse() {
	let input = "8,10
9,0

fold along y=7";
	assert_eq!(parse(input), (HashSet::from([(8,10),(9,0)]), vec![('y', 7)]));
    }

    #[test]
    fn test_apply() {
	let points = HashSet::from([(0,0), (2,3), (0,4)]);
	let result = apply(points, ('y', 2));
	assert_eq!(result, HashSet::from([(0,0), (2,1)]));
    }

    #[test]
    fn test_part_a() {
	let input = "6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5";

	assert_eq!(part_a(input), "17");
    }
}
