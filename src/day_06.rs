use std::collections::HashMap;

fn step(counts: HashMap<i32, u64>) -> HashMap<i32, u64> {
    let mut new: HashMap<i32, u64> = HashMap::new();

    for i in 0..=7 {
        new.insert(i, *counts.get(&(i + 1)).unwrap_or(&0));
    }

    let at_0 = *counts.get(&0).unwrap_or(&0);
    new.insert(8, at_0);
    *new.entry(6).or_insert(0) += at_0;

    new
}

fn steps(counts: HashMap<i32, u64>, n: i32) -> HashMap<i32, u64> {
    if n == 0 {
        return counts;
    };

    steps(step(counts), n - 1)
}

fn main(data: &str, n: i32) -> u64 {
    let result = data
        .trim()
        .split(',')
        .map(|s| s.parse::<i32>().unwrap())
        .collect::<Vec<_>>();

    let mut counts: HashMap<i32, u64> = HashMap::new();

    for r in result {
        *counts.entry(r).or_insert(0) += 1;
    }

    steps(counts, n).values().sum()
}

pub fn part_a(data: &str) -> String {
    main(data, 80).to_string()
}

pub fn part_b(data: &str) -> String {
    main(data, 256).to_string()
}

#[cfg(test)]
mod tests {
    use super::{super::utils, *};

    #[test]
    fn assert_true() {
        assert_eq!(super::main("3,4,3,1,2", 80), 5934)
    }

    #[test]
    fn run() {
        let input = utils::input("day_06");
        println!("{:?}", part_a(&input));
        println!("{:?}", part_b(&input));
    }
}
