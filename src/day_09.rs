use itertools::Itertools;
use std::{
    collections::{HashMap, HashSet},
    hash::Hash,
    ops::Add,
};

#[derive(Hash, PartialEq, Eq, Debug, Clone)]
pub struct Pair {
    x: i32,
    y: i32,
}

impl Add for Pair {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl Pair {
    fn neighbours(&self) -> Vec<Self> {
        let deltas = vec![
            Pair { x: -1, y: 0 },
            Pair { x: 0, y: -1 },
            Pair { x: 0, y: 1 },
            Pair { x: 1, y: 0 },
        ];
        deltas
            .into_iter()
            .map(|d| d + self.clone())
            .collect()
    }
}

pub struct Map {
    points: HashMap<Pair, i32>,
}

impl Map {
    pub fn is_minimum(&self, p: &Pair) -> bool {
        let val = self.points.get(p).unwrap();
        p.neighbours()
            .iter()
            .filter(|n| self.points.get(n).unwrap_or(&9) > val)
            .count()
            == 4
    }

    pub fn minimum_points(&self) -> Vec<&Pair> {
        self.points.keys().filter(|&p| self.is_minimum(p)).collect()
    }

    pub fn minimums(&self) -> Vec<&i32> {
        self.minimum_points()
            .iter()
            .map(|p| self.points.get(p).unwrap())
            .collect()
    }

    pub fn bassin(&self, p: &Pair) -> HashSet<Pair> {
        let mut bassin: HashSet<Pair> = HashSet::new();
        let mut current: HashSet<Pair> = HashSet::new();
        current.insert(p.clone());
	
        while !current.is_empty() {
            for i in &current {
                bassin.insert(i.clone());
            }
            current = current
                .iter()
                .flat_map(|p| p.neighbours())
                .filter(|p| *self.points.get(p).unwrap_or(&9) < 9)
                .collect::<HashSet<_>>()
                .difference(&bassin)
                .cloned()
                .collect();
        }
        bassin
    }
}

pub fn parse(input: &str) -> HashMap<Pair, i32> {
    let mut map = HashMap::new();
    for (y, line) in input.lines().enumerate() {
        for (x, chr) in line.chars().enumerate() {
            let pair = Pair {
                x: x as i32,
                y: y as i32,
            };
            map.insert(pair, chr.to_string().parse().unwrap());
        }
    }
    map
}

pub fn part_a(input: &str) -> String {
    let points = parse(input);
    let map = Map { points };
    let minimums = map.minimums();
    let sum: i32 = minimums.iter().map(|&i| *i).sum();
    (sum + minimums.len() as i32).to_string()
}

pub fn part_b(input: &str) -> String {
    let points = parse(input);
    let map = Map { points };

    map.minimum_points()
        .iter()
        .map(|p| map.bassin(p).len())
        .sorted()
        .rev()
        .take(3)
        .product::<usize>()
        .to_string()
}

#[cfg(test)]
mod tests {
    use super::{super::utils, *};

    fn test_input() -> String {
        "2199943210
3987894921
9856789892
8767896789
9899965678"
            .to_string()
    }

    #[test]
    fn run() {
        println!("{}", part_a(&test_input()));
        println!("{}", part_a(&utils::input("day_09")));

        println!("{}", part_b(&test_input()));
        println!("{}", part_b(&utils::input("day_09")));
    }

    #[test]
    fn test_parses() {
        assert_eq!(parse(&test_input()).len(), 50);
    }

    #[test]
    fn test_neighbours() {
        let point = Pair { x: 2, y: 0 };
        assert_eq!(
            point.neighbours(),
            vec![
                Pair { x: 1, y: 0 },
                Pair { x: 2, y: -1 },
                Pair { x: 2, y: 1 },
                Pair { x: 3, y: 0 }
            ]
        );
    }

    #[test]
    fn test_local_minimum() {
        let map = Map {
            points: parse(&test_input()),
        };

        let point1 = Pair { x: 1, y: 0 };
        assert!(map.is_minimum(&point1));

        let point2 = Pair { x: 2, y: 0 };
        assert!(!map.is_minimum(&point2));
    }

    #[test]
    fn test_minimums() {
        let map = Map {
            points: parse(&test_input()),
        };

        assert_eq!(map.minimums().len(), 4)
    }

    #[test]
    fn test_bassin() {
        let map = Map {
            points: parse(&test_input()),
        };

        let point1 = Pair { x: 3, y: 2 };
        assert_eq!(map.bassin(&point1).len(), 14);
    }
}
