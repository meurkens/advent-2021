use std::fs;

pub fn input(file: &str) -> String {
    fs::read_to_string(format!("input/{}", file)).unwrap()
}
