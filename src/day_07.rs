use std::collections::HashMap;

fn parse(data: &str) -> HashMap<i32, i32> {
    let numbers: Vec<i32> = data.trim().split(',').map(|s| s.parse().unwrap()).collect();

    let mut result = HashMap::new();

    for n in numbers {
        *result.entry(n).or_insert(0) += 1;
    }

    result
}

pub fn part_a(data: &str) -> String {
    let input = parse(data);
    let length: i32 = input.values().sum();
    let mut passed = 0;
    let mut median = -1;
    while passed < length / 2 {
        median += 1;
        passed += input.get(&median).unwrap_or(&0);
    }

    let result: i32 = input
        .keys()
        .map(|n| (median - n).abs() * input.get(n).unwrap())
        .sum();

    result.to_string()
}

fn efforts(iterator: impl Iterator<Item = i32>) -> Vec<i32> {
    let mut output: Vec<i32> = Vec::new();
    let mut force = 0;
    let mut submarines = 0;
    let mut total = 0;

    // TODO: iterator.cumultative().cumultative().cumultative()
    for n in iterator {
        output.push(total);
        submarines += n;
        force += submarines;
        total += force;
    }
    output
}

fn build_iterator(input: &HashMap<i32, i32>) -> Vec<i32> {
    let from = input.keys().min().unwrap();
    let to = input.keys().max().unwrap();

    (*from..=*to)
        .map(|x| *input.get(&x).unwrap_or(&0))
        .collect()
}

fn from_left(input: &HashMap<i32, i32>) -> Vec<i32> {
    efforts(build_iterator(input).into_iter())
}

fn from_right(input: &HashMap<i32, i32>) -> Vec<i32> {
    efforts(build_iterator(input).into_iter().rev())
}

fn run2(input: HashMap<i32, i32>) -> i32 {
    from_left(&input)
        .iter()
        .zip(from_right(&input).iter().rev())
        .map(|(x, y)| x + y)
        .min()
        .unwrap()
}

pub fn part_b(data: &str) -> String {
    run2(parse(data)).to_string()
}

#[cfg(test)]
mod tests {
    use super::{super::utils, *};

    #[test]
    fn run() {
        let input_demo = "16,1,2,0,4,2,7,1,2,14";

        println!("{}", part_a(input_demo));
        println!("{}", part_a(&utils::input("day_07")));
        println!("{}", part_b(input_demo));
        println!("{}", part_b(&utils::input("day_07")));
    }

    #[test]
    fn test_form_left() {
        assert_eq!(from_left(&parse("0,3")), vec![0, 1, 3, 6]);
        assert_eq!(from_left(&parse("0,1,3")), vec![0, 1, 4, 9]);
    }

    #[test]
    fn test_from_right() {
        assert_eq!(from_right(&parse("0,1,3")), vec![0, 1, 3, 7]);
    }
}
