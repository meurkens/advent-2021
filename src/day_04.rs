use std::collections::HashMap;

#[derive(Debug)]
struct Card {
    numbers: HashMap<i32, usize>,
    rows: [i32; 5],
    columns: [i32; 5],
    found: Vec<i32>,
    finished: bool,
}

impl Card {
    fn strike(&mut self, number: i32) -> Option<i32> {
        let found = self.numbers.get(&number);
        match found {
            Some(n) => {
                let x = n % 5;
                let y = n / 5;
                self.rows[y] += 1;
                self.columns[x] += 1;
                self.found.push(number);
                self.check()
            }
            _ => None,
        }
    }

    fn check(&mut self) -> Option<i32> {
        let bingo = self.rows.contains(&5) || self.columns.contains(&5);
        if self.finished || !bingo {
            return None;
        };

        self.finished = true;

        let total: i32 = self.numbers.keys().sum();
        let last: i32 = *self.found.last().unwrap();
        let sum_found: i32 = self.found.iter().sum();
        let result: i32 = last * (total - sum_found);
        Some(result)
    }
}

fn build_card(numbers: Vec<i32>) -> Card {
    let mut numbers_hash = HashMap::new();

    for (i, n) in numbers.iter().enumerate() {
        numbers_hash.insert(*n, i as usize);
    }

    Card {
        numbers: numbers_hash,
        rows: [0; 5],
        columns: [0; 5],
        found: vec![],
        finished: false,
    }
}

fn parse_card(data: &str) -> Vec<i32> {
    data.split_whitespace()
        .map(|s| s.parse().unwrap())
        .collect()
}

fn parse(data: &str) -> (Vec<i32>, Vec<Card>) {
    let paragraphs: Vec<_> = data.split("\n\n").collect();

    let numbers = paragraphs[0]
        .split(',')
        .map(|x| x.parse().unwrap())
        .collect();

    let cards = paragraphs[1..]
        .iter()
        .map(|s| build_card(parse_card(s)))
        .collect();

    (numbers, cards)
}

fn main(data: &str) -> Vec<i32> {
    let (numbers, mut cards): (Vec<i32>, Vec<Card>) = parse(data);
    let mut answers: Vec<i32> = vec![];

    for n in numbers {
        for c in &mut cards {
	    if let Some(r) = c.strike(n) {
		answers.push(r)
	    }
        }
    }
    answers
}

pub fn part_a(input: &str) -> String {
    main(input).first().unwrap().to_string()
}

pub fn part_b(input: &str) -> String {
    main(input).last().unwrap().to_string()
}
