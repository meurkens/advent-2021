fn parse_line(line: &str) -> Vec<i32> {
    line.chars()
        .map(|s| s.to_digit(10).unwrap() as i32)
        .collect()
}

fn combine(l: &[i32], r: &[i32]) -> Vec<i32> {
    l.iter().zip(r.iter()).map(|(a, b)| a + b).collect()
}

fn most(lines: &[Vec<i32>], pos: i32) -> i32 {
    let sum: i32 = lines.iter().map(|l| l[pos as usize]).sum();
    let halve = (lines.len() + 1) / 2;
    if (sum as usize) >= halve {
        1
    } else {
        0
    }
}

fn find(lines: Vec<Vec<i32>>, pos: i32) -> Vec<Vec<i32>> {
    if lines.len() == 1 {
        return lines;
    };

    let most_at_pos = most(&lines, pos);
    find(
        lines
            .into_iter()
            .filter(|l| l[pos as usize] == most_at_pos)
            .collect(),
        pos + 1,
    )
}

fn find_least(lines: Vec<Vec<i32>>, pos: i32) -> Vec<Vec<i32>> {
    if lines.len() == 1 {
        return lines;
    };

    let most_at_pos = most(&lines, pos);
    find_least(
        lines
            .into_iter()
            .filter(|l| l[pos as usize] != most_at_pos)
            .collect(),
        pos + 1,
    )
}

fn binary_to_int(binary: &[i32]) -> i32 {
    let len = binary.len();
    if len == 0 {
        return 0;
    };
    binary.last().unwrap() + 2 * binary_to_int(&binary[0..len - 1])
}

pub fn part_b(input: &str) -> String {
    let binaries = input.lines().map(|s| parse_line(s));

    let oxygen_generator_rating = binary_to_int(&find(binaries.clone().collect(), 0)[0]);
    let co2_scrubber_rating = binary_to_int(&find_least(binaries.collect(), 0)[0]);

    (oxygen_generator_rating * co2_scrubber_rating).to_string()
}

pub fn part_a(input: &str) -> String {
    let halve = input.lines().count() / 2;
    let binaries = input.lines().map(|s| parse_line(s));
    let binary: String = binaries
        .clone()
        .reduce(|acc, x| combine(&acc, &x))
        .unwrap()
        .iter()
        .map(|n| if (*n as usize) > halve { '1' } else { '0' })
        .collect();

    let result = i32::from_str_radix(&binary, 2).unwrap();
    let max = 2_i32.pow(binary.len().try_into().unwrap()) - 1;

    ((max - result) * result).to_string()
}
