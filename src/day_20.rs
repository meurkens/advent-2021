use std::collections::HashMap;

use itertools::Itertools;

const NEIGHBOURS: [(i64, i64); 9] = [
    (-1, -1),
    (0, -1),
    (1, -1),
    (-1, 0),
    (0, 0),
    (1, 0),
    (-1, 1),
    (0, 1),
    (1, 1),
];

#[derive(Clone)]
struct World {
    map: HashMap<(i64, i64), char>,
    size: ((i64, i64), (i64, i64)),
}

fn parse(input: &str) -> (World, Vec<char>) {
    let (key_input, world_input) = input.split_once("\n\n").unwrap();

    let key = key_input
        .chars()
        .map(|c| if c == '#' { '1' } else { '0' })
        .collect();

    let mut map = HashMap::new();

    let mut max_x: i64 = 0;
    let mut max_y: i64 = 0;
    for (y, line) in world_input.lines().enumerate() {
        for (x, c) in line.chars().enumerate() {
            let value = if c == '#' { '1' } else { '0' };
            map.insert((x as i64, y as i64), value);
            max_x = x as i64;
        }
        max_y = y as i64;
    }

    let world = World {
        map,
        size: ((0, max_x), (0, max_y)),
    };

    (world, key)
}

fn step(world: &World, key: &[char], default: char) -> World {
    let ((x_min, x_max), (y_min, y_max)) = world.size;

    let mut result = HashMap::new();

    for x in (x_min - 1)..=(x_max + 1) {
        for y in (y_min - 1)..=(y_max + 1) {
            let binary = NEIGHBOURS
                .iter()
                .map(|(dx, dy)| (x + dx, y + dy))
                .map(|pos| world.map.get(&pos).unwrap_or(&default))
                .join("");

            let integer = usize::from_str_radix(&binary, 2).unwrap();
            let value = key[integer];

            result.insert((x, y), value);
        }
    }

    World {
        map: result,
        size: ((x_min - 1, x_max + 1), (y_min - 1, y_max + 1)),
    }
}

fn steps(map: &World, key: &[char], n: i32) -> World {
    let mut world = map.clone();
    for i in 1..=n {
        let default = if i % 2 == 1 { '0' } else { key[0] };
        world = step(&world, key, default);
    }
    world
}

/*
fn print(world: &World) {
    let mut result = "".to_string();

    let &x_min = world.keys().map(|(x, _)| x).min().unwrap();
    let &x_max = world.keys().map(|(x, _)| x).max().unwrap();
    let &y_min = world.keys().map(|(_, y)| y).min().unwrap();
    let &y_max = world.keys().map(|(_, y)| y).max().unwrap();

    for y in y_min..=y_max {
        for x in x_min..=x_max {
            let &c = world.get(&(x, y)).unwrap();
            let c = if c == '1' { '#' } else { '.' };
            result.push(c);
        }
        result.push('\n');
    }

    println!("{}", result);
}
*/

pub fn part_a(input: &str) -> String {
    let (world, key) = parse(input);

    steps(&world, &key, 2)
        .map
        .values()
        .filter(|&&x| x == '1')
        .count()
        .to_string()
}

pub fn part_b(input: &str) -> String {
    let (world, key) = parse(input);

    steps(&world, &key, 50)
        .map
        .values()
        .filter(|&&x| x == '1')
        .count()
        .to_string()
}

#[cfg(test)]
mod tests {
    use super::super::utils;
    use super::*;

    const DEMO_INPUT: &str = "..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#

#..#.
#....
##..#
..#..
..###";

    #[test]
    fn run() {
        println!("{}", part_a(&utils::input("day_20")));
        println!("{}", part_b(&utils::input("day_20")));
    }

    #[test]
    fn test() {
        println!("{}", part_a(DEMO_INPUT));
        println!("{}", part_b(DEMO_INPUT));
    }
}
